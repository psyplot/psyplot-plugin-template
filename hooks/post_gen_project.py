# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

import os
import shutil
import sys
import git
import subprocess as spr
import glob
import requests


from packaging import version

import reuse

REUSE_V5 = version.parse(reuse.__version__) >= version.parse("5.0")


{% if cookiecutter.use_markdown_for_documentation == "yes" %}
for fname in glob.glob("docs/*.rst"):
    os.remove(fname)
{% else %}
for fname in glob.glob("docs/*.md"):
    os.remove(fname)
{% endif %}

{% if cookiecutter.ci_matrix != "pipenv" and cookiecutter.use_cibuildwheel == "no" %}
# remove pipenv ci matrix
shutil.rmtree("ci")
{% elif cookiecutter.ci_matrix != "pipenv" %}
shutil.rmtree("ci/matrix")
{% elif cookiecutter.use_cibuildwheel == "no" %}
os.remove("ci/.gitlab-ci.build.yml")
{% endif %}

# -----------------------------------------------------------------------------
# ------------ read environment variables -------------------------------------
# -----------------------------------------------------------------------------

# check if we should be silent
if os.getenv("SILENT_HOOKS"):
    silent = True
    spr_kwargs = dict(stdout=spr.DEVNULL, stderr=spr.STDOUT)
else:
    silent = False
    spr_kwargs = {}

# check if the project is initialized in an already existing git repo
if os.getenv("PARENT_GIT_REPO"):
    parent = os.getenv("PARENT_GIT_REPO")
else:
    parent = None

# check if we should run pre-commit hooks in the beginning
skip_pre_commit = parent is not None or os.getenv("SKIP_PRE_COMMIT")

{% if cookiecutter.use_reuse == "yes" %}

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "docs",
        "--recursive",
        "README.md",
    ] + glob.glob("docs/*.rst") + glob.glob("docs/*.md") + glob.glob("docs/_static/*") + glob.glob("docs/_templates/*"),
    **spr_kwargs,
)

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "supp",
        "--recursive",
        ".gitignore",
        ".pre-commit-config.yaml",
        "pyproject.toml",
        "setup.py",
        ".gitattributes",
        ".gitlab-ci.yml",
        ".readthedocs.yaml",
        ".vscode/settings.json",
        "CHANGELOG.md",
        "MANIFEST.in",
        "Makefile",
        "tox.ini",
        "docs/Makefile",
        "docs/make.bat",
        "docs/requirements.txt",
    ],
    **spr_kwargs,
)
args = [
    sys.executable,
    "-m",
    "reuse_shortcuts",
    "supp",
    "--style",
    "python",
    ".flake8",
    "CITATION.cff",
    ".reuse/shortcuts.yaml",
    {% if cookiecutter.ci_matrix == "pipenv" %}
    "ci/matrix/default/Pipfile",
    {% endif %}
    {% if cookiecutter.use_cibuildwheel == "yes" %}
    "ci/.gitlab-ci.build.yml",
    {% endif %}
]
if not REUSE_V5:
    args.append("--skip-unrecognised")

spr.check_call(
    args,
    **spr_kwargs,
)

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "code",
        "--recursive",
        "{{ cookiecutter.package_folder }}",
        "conftest.py",
        "docs/conf.py",
        "tests",
    ],
    **spr_kwargs,
)

if not silent:
    print("Download license image")
response = requests.get(
    "http://mirrors.creativecommons.org/presskit/buttons/88x31/png/{{ cookiecutter.documentation_license.split('-', 1)[1].rsplit('-', 1)[0].lower() }}.png"
)
os.makedirs("docs/_static", exist_ok=True)
with open("docs/_static/license_logo.png", "wb") as f:
    f.write(response.content)
spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{% now 'utc', '%Y' %}",
        "--license",
        "CC-BY-4.0",
        "--copyright",
        "Creative Commons",
        "docs/_static/license_logo.png",
    ],
    **spr_kwargs,
)
spr.check_call(["reuse", "download", "--all"], **spr_kwargs)

{% endif %}

if not silent:
    print("Fixing files for project at {{cookiecutter.project_slug}}")

if parent:
    repo = git.Repo(parent)
else:
    repo = git.Repo.init(".", mkdir=False)
    {% if cookiecutter.git_remote_protocoll == "ssh" %}
    repo.create_remote(
        "origin",
        "git@{{ cookiecutter.gitlab_host }}:{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git",
    )
    {% else %}
    repo.create_remote(
        "origin",
        "https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git",
    )
    {% endif %}

repo.git.add(".")


spr.check_call(["pre-commit", "install"], **spr_kwargs)

# make a silent run first to fix the files
if not skip_pre_commit:
    spr.call(["pre-commit", "run"], stdout=spr.DEVNULL)

# add the files again after pre-commit hook has been run
repo.git.add(".")

if not skip_pre_commit:
    spr.check_call(["pre-commit", "run"], **spr_kwargs)

for dirpath, dirnames, filenames in os.walk("."):
    if "__pycache__" in dirnames:
        shutil.rmtree(os.path.join(dirpath, "__pycache__"))

{%- set counter = count(1) %}

if not silent:

    print("""
======================================================================
                        Contratulations!
======================================================================
You just created a new state-of-the-art python package.

{% if cookiecutter.use_reuse == "yes" -%}
+-------------------------------------------------------------+
| IMPORTANT NOTE:                                             |
|                                                             |
|    If you used cruft to create this package, please now run |
|    the following commands to stage the .cruft.json file and |
|    assign it the correct license:                           |
|                                                             |
+-------------------------------------------------------------+

{{ counter | next }}. Set the correct license information:

    $ reuse --root {{ cookiecutter.project_slug }} annotate --year {{ cookiecutter.copyright_year }} --license {{ cookiecutter.supplementary_files_license }} --copyright "{{ cookiecutter.copyright_holder }}" {{ cookiecutter.project_slug }}/.cruft.json

{{ counter | next }}. Stage the file:

    $ git -C {{ cookiecutter.project_slug }} add .cruft.json .cruft.json.license

{%- else %}
+-------------------------------------------------------------+
| IMPORTANT NOTE:                                             |
|                                                             |
|    You can now push everything to gitlab!                   |
|                                                             |
+-------------------------------------------------------------+

{{ counter | next }}. Stage the file .cruft.json:

    $ git -C {{ cookiecutter.project_slug }} add .cruft.json

{%- endif %}

{{ counter | next }}. Make your first commit via

    $ git -C {{ cookiecutter.project_slug }} commit -m "Initial commit"

{{ counter | next }}. Create the repository at https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
   via

    $ git -C {{ cookiecutter.project_slug }} push -u origin main

   (You might need to create it through the web-interface first).

{% if cookiecutter.deploy_pages_in_ci == "git-push" %}

{{ counter | next }}. Create a access token for deploying the docs in the CI at

    https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/-/settings/access_tokens

   with Developer role and the scopes 'read_repository' and 'write_repository'

{{ counter | next }}. Add this token to the CI/CD-variables at

    https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/-/settings/ci_cd#js-cicd-variables-settings

   with the key CI_DEPLOY_TOKEN, and check the check box at 'Mask variable',
   'Protect variable' and do not check 'Expand variable reference'

{%- endif %}

{{ counter | next }}. Finally, we recommend that you register this package at
   https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview
   such that the maintainers of the template can support you with keeping your
   skeleton up-to-date. You can do this by opening an issue with the following
   URL:

    https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview/-/issues/new?issue%5Bconfidential%5D=true&issue%5Btitle%5D=New%20template%20usage%20for%20{{ cookiecutter.project_slug }}%20from%20{{ cookiecutter._template.split('/')[-1] }}&issue%5Bdescription%5D=Dear%20HCDC%20support%2C%20I%27d%20like%20to%20register%20the%20following%20repository%0A%0Ahttps%3A//{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git%0A%0Athat%20is%20using%20the%20following%20template%0A%0A{{ cookiecutter._template.replace('https:', 'https%3A' )}}"
""")
