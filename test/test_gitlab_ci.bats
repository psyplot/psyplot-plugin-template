# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

#  test the created .gitlab-ci.yml file for different setups.
#
# note that all of these tests require the following environment variables:
#
# CI_API_V4_URL: The url for the gitlab API (is set automatically in Gitlab CI)
# GITLAB_API_TOKEN: A token with api scope to access the gitlab API and lint the files
#
# If any of these variables are not set, all the tests here are skipped

setup() {
    load 'test_helper/common-setup'
    _common_setup


    if [ ! "$CI_API_V4_URL" ]; then
      skip "Skipping due to missing gitlab API url"
    fi

    if [ ! "$GITLAB_API_TOKEN" ]; then
        skip "Skipping due to missing gitlab API token"
    fi

}

@test "test linting the gitlab ci (no deploy)" {
    create_template '{deploy_package_in_ci: "no", deploy_pages_in_ci: "no"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (package deploy)" {
    create_template '{deploy_package_in_ci: "yes", deploy_pages_in_ci: "no"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pages deploy)" {
    create_template '{deploy_package_in_ci: "no", deploy_pages_in_ci: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pages deploy via git-push)" {
    create_template '{deploy_package_in_ci: "no", deploy_pages_in_ci: "git-push"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pipenv)" {
    create_template '{ci_matrix: "pipenv"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pytest-xdist)" {
    create_template '{use_pytest_xdist: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pipenv, pytest-xdist)" {
    create_template '{ci_matrix: "pipenv", use_pytest_xdist: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (gui)" {
    create_template '{requires_gui: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (pipenv and gui)" {
    create_template '{ci_matrix: "pipenv", requires_gui: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (default)" {
    create_template
    lint_gitlab_ci
}

@test "test linting the gitlab ci (ci_build_stage)" {
    create_template '{ci_build_stage: "yes"}'
    lint_gitlab_ci
}

@test "test linting the gitlab ci (cibuildwheel)" {
    create_template '{use_cibuildwheel: "yes"}'
    # replace include statement and replace with included file
    sed -i '/^include:/d' $PROJECT_FOLDER/.gitlab-ci.yml
    cat $PROJECT_FOLDER/ci/.gitlab-ci.build.yml >> $PROJECT_FOLDER/.gitlab-ci.yml
    lint_gitlab_ci
}

@test "test linting the gitlab ci (ci_build_stage, cibuildwheel)" {
    create_template '{ci_build_stage: "yes", use_cibuildwheel: "yes"}'
    # replace include statement and replace with included file
    sed -i '/^include:/d' $PROJECT_FOLDER/.gitlab-ci.yml
    cat $PROJECT_FOLDER/ci/.gitlab-ci.build.yml >> $PROJECT_FOLDER/.gitlab-ci.yml
    lint_gitlab_ci
}

@test "test linting the gitlab ci (ci_build_stage, cibuildwheel, pipenv)" {
    create_template '{ci_build_stage: "yes", use_cibuildwheel: "yes", ci_matrix: "pipenv"}'
    # replace include statement and replace with included file
    sed -i '/^include:/d' $PROJECT_FOLDER/.gitlab-ci.yml
    cat $PROJECT_FOLDER/ci/.gitlab-ci.build.yml >> $PROJECT_FOLDER/.gitlab-ci.yml
    lint_gitlab_ci
}
