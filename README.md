<!--
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

# Cookiecutter template for a psyplot plugin

[![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/psyplot/psyplot-plugin-template)](https://api.reuse.software/info/codebase.helmholtz.cloud/psyplot/psyplot-plugin-template)
[![License](https://img.shields.io/gitlab/license/psyplot%2Fpsyplot-plugin-template?gitlab_url=https%3A%2F%2Fcodebase.helmholtz.cloud)](https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template#license-information)

This repository provides the possibility to generate the basic structure
of an python package with state-of-the-art best-practices research
software engineer methods.

## Usage

This template can be used to create a new python package, and to update
a python package that has been created with this template. The template
is made for the
[cookiecutter][cookiecutter] library,
but in order to be able to update packages that have been created with
this template, we will be using [cruft][cruft].

[cookiecutter]: https://cookiecutter.readthedocs.io/en/stable/
[cruft]: https://cruft.github.io/cruft/

## Prerequisites

Make sure that you have the requirements in [requirements.txt](requirements.txt)
installed. You can do so by downloading it and running

``` bash
pip install -r requirements.txt
```

### Package creation

You can generate a new python package with the following commands:

``` bash
cruft create --skip .git --skip .mypy_cache https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template.git
```

This will prompt for some input, including a `project_slug` under which
you will then find the skeleton of a new python package. See the
[parameters.rst](parameters.rst) file for information on the individual
configuration parameters and possible environment variables.

The newly created folder will be setup as a git repository and the new
files will be staged automatically. It will run all the formatters for
the package and ensures high quality of your code right from the
beginning.

You will then receive some final instructions on how to finish the
initial setup.

> **Note**
>
> You can also clone this repository via `git clone`, but we recommend to
> use the URL. Otherwise `cruft` will store the path to the local folder
> in the `.cruft.json` configuration file.


### Register template usage

When you created a package with this template, please register it at
https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview/.

This helps the maintainers of the template to get an overview and to support
you with the usage of the template.


### Package update

Code-quality tools and frameworks quickly evolved in recent years and it
is indeed quite challenging to stay up-to-date with the latest
developments. By using [cruft][cruft], we
ensure that you can focus on your code and outsource these code-quality
developments to this template.

To update a package that has been created with this template and
`cruft`,

1.  Change into the directory for the package that you created

    ``` bash
    cd <path-to-you-package>
    ```

2.  Create a new branch here (let\'s say `update-skeleton`):

    ``` bash
    git branch update-skeleton
    git checkout update-skeleton
    ```

3.  use the `update` command of `cruft`, review the changes and apply
    them:

    ``` bash
    cruft update
    ```

4.  Make sure that everything is working by executing the test suite:

    ``` bash
    git add .
    pre-commit run
    # resolve the issues that pre-commit may bring up and ones it is working,
    # run the test suite
    make dev-install
    tox
    ```

5.  If everything is working, commit the changes

    ``` bash
    git commit -m "Updated package skeleton"
    ```

6.  Now push the changes to the remote

    ``` bash
    git push origin update-skeleton
    ```

    and create a merge request.


## About the tools that your package will use

Packages that are built with this template repository do automatically
use the following state-of-the-art tools for automated code formatting
and validation.

If you have any questions or troubles with these tools, please contact
the maintainers of this template rather sooner than later. You can
[open an issue in this repository][issues] or contact us at
<hcdc_support@hereon.de>.

[issues]: https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template/-/issues

### Formatters

Automated code formatters make your development faster and easier as you
do not have to worry about how to make your code readable. The formatter
cares about this. Packages that are built with this template have the
following formatters configured:

-   [black][black] for standardized code formatting
-   [blackdoc][blackdoc] for standardized code formatting in documentation
-   [isort][isort] for standardized order in imports.

To run all formatters, we recommend that you stage the files (e.g. via
`git add`) and use `pre-commit run` (see below). This will format the
files and you can compare them to the staged versions.

Additionally we add a `vscode/settings.json` that uses `black` as the
default formatter and automatically applies the formatting when you save
the file (as such, just save your files as often as possible and they
will be formatted automatically).

[black]: https://black.readthedocs.io/en/stable/
[blackdoc]: https://blackdoc.readthedocs.io/en/latest/
[isort]: https://github.com/PyCQA/isort

### Testing frameworks

The package uses two testing frameworks: [pre-commit][pre-commit] and
[tox][tox].

[pre-commit]: https://pre-commit.com/
[tox]: https://tox.wiki

#### `pre-commit`

[pre-commit](#pre-commit) can be installed directly in the git system
and validates the code prior to every commit. You will find more
information about this in the `docs/contributing.md` or
`docs/contributing.rst` file of the generated repository.

#### `tox`

[tox][tox] standardizes the invocation of your test suite. It creates a
virtual environment, installs the packages and runs the specified tests
that are based on the validators below. Additionally it invokes a call
of [pytest][pytest] to look for unit or integration tests
that you wrote for your software (see `tests/test_imports.py` for
instance).

[pytest]: https://pytest.org

### Validators

The package uses static code validators to ensure that the coding style
follows good practices and to prevent errors in the code. These
validators are implemented in the configs for `pre-commit` and `tox`.

The generated package uses the following static code validators:

-   [mypy][mypy] for static type checking on [type
    hints](https://docs.python.org/3/library/typing.html)
-   [flake8][flake8] for general code quality
-   [reuse][reuse] for handling of licenses
-   [cffconvert][cffconvert] for validating the `CITATION.cff` file.

[mypy]: http://mypy-lang.org/
[flake8]: http://flake8.pycqa.org/en/latest/
[reuse]: https://reuse.readthedocs.io/
[cffconvert]: https://github.com/citation-file-format/cff-converter-python

#### static type analysis via mypy

`mypy` validates the type hints in your code. Using type hints makes
your code better as every reader of your code can immediately see what
inputs and outputs you expect. However, `mypy` is not perfect and it can
sometimes be difficult to implement a strict typing in an interpreted
language such as python. If `mypy` causes an error and you are not sure
about how to solve it, you should add a `# type: ignore` statement on
that line. This will tell `mypy` to ignore the error messages that it
would produce for the given line in the code.

#### Code quality via flake8

flake8 is used to make sure that your code has a good quality and
follows PEP8 conventions. Most of the checks that flake8 does are
already covered by the `black` formatter, so you do not have to worry
about them. The most important point for `flake8` is that it tells you
if you have variables or imports that you do not use. This helps you
keeping your code clean.

#### reuse

On every commit, `reuse` validates every file in your repository to test
whether there are correctly encoded license information. As such you
need to call [reuse annotate][annotate] on
every new file that you create (unless it is excluded from the version
control via the `.gitignore` file).

[annotate]: https://reuse.readthedocs.io/en/latest/usage.html#annotate

#### cffconvert

The generated package has a `CITATION.cff` file for best practices in
referencing your software and helps you getting credit for your work.
You should review this file once it has been generated and maybe add the
orcids of the authors and maintainers.

The citation file format is a powerful standard with several fields that
you can fill out (we fill here only more or less the minimum). You
should have a look into the
[documentation](https://citation-file-format.github.io/) and checkout
the other fields.

### Testing

We strongly recommend that you write as many unit tests for your
software as possible. This will not only make your code more
sustainable, it will also help you when you need to update the skeleton
with `cruft update`.

The package that you generated is already setup for this. It is using
[tox](#tox) and [pytest](https://pytest.org) to discover and run your
test suite. Add your tests to the `tests` folder, and you will be on the
safe side.

## Extending this template

This template is a very basic template for an arbitrary python package.
Depending on the libraries and frameworks that you use, you might want
to use this repository as a starting point to create your own template.
If you want to do so, you should [create a fork of this repository][fork]
and implement the changes or additions in your fork. By doing so, you
can always merge changes from this repository into your fork and stay
up-to-date with the code quality tools that we use here.

If you want to directly make improvements to this template, please
[do so in a merge request at the source code repository][merge-request]
or [create an issue][issue].

[fork]: https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template/-/forks/new
[merge-request]: https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template/-/merge_requests/new
[issue]: https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template/-/issues


## License Information

If not specified otherwise in the header of the file or a `.license` file
next to it, the template files in this repository (i.e. everything in the
`{{cookiecutter.project_slug}}` folder) are licensed under Public Domain
(`CC0-1.0`). See `.reuse/dep5` for further information.

For other files, please refer to the header of the corresponding file.

All license files are located in the [LICENSES](LICENSES) folder at the root
of the repository.

### Running tests

This repo is using [`bats`](https://bats-core.readthedocs.io/en/stable/) for
automated tests. `bats` is contained as git submodules in the source code
repository. So if you clone this repo via

```bash
git clone --recursive https://codebase.helmholtz.cloud/psyplot/psyplot-plugin-template.git
```

or run a

```bash
git submodule update --init
```

once you cloned it and installed the [requirements.txt](requirements.txt), you
can run the test suite via

```bash
./test/bats/bin/bats test/
```

Note that this command needs to be enquired from a bash shell (e.g. `Git Bash`
on windows).

See also the setup in the [gitlab CI config file](.gitlab-ci.yml) for a working
setup.
