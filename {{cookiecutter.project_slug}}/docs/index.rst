.. {{ cookiecutter.project_slug }} documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

{{ underlined("Welcome to {}'s documentation!", cookiecutter.project_slug, symbol="=") }}

|CI|
|Code coverage|
{%- if 'readthedocs' in cookiecutter.documentation_url %}|Docs| {% endif %}
|Latest Release|
|PyPI version|
|Code style: black|
|Imports: isort|
|PEP8|
|Checked with mypy|
{% if cookiecutter.use_reuse %}|REUSE status|{% endif %}

.. rubric:: {{ cookiecutter.project_short_description }}

.. warning::

    This page has been automatically generated as has not yet been reviewed by
    the authors of {{ cookiecutter.project_slug }}!
    Stay tuned for updates and discuss with us at
    https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   api
   contributing


How to cite this software
-------------------------

.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


License information
-------------------
Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

The source code of {{ cookiecutter.project_slug }} is licensed under
{{ cookiecutter.code_license }}.

If not stated otherwise, the contents of this documentation is licensed under
{{ cookiecutter.documentation_license }}.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |CI| image:: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/badges/main/pipeline.svg
   :target: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/-/pipelines?page=1&scope=all&ref=main
.. |Code coverage| image:: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/badges/main/coverage.svg
   :target: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/-/graphs/main/charts
{%- if 'readthedocs' in cookiecutter.documentation_url %}
.. TODO: uncomment the following line when the documentation is published at https://readthedocs.org
.. .. |Docs| image:: https://readthedocs.org/projects/{{ cookiecutter.project_slug }}/badge/?version=latest
..    :target: {{ cookiecutter.documentation_url }}
{%- endif %}
.. |Latest Release| image:: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/-/badges/release.svg
   :target: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
.. .. TODO: uncomment the following line when the package is published at https://pypi.org
.. .. |PyPI version| image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_slug }}.svg
..    :target: https://pypi.python.org/pypi/{{ cookiecutter.project_slug }}/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |Checked with mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
{%- if cookiecutter.use_reuse %}
.. TODO: uncomment the following line when the package is registered at https://api.reuse.software
.. .. |REUSE status| image:: https://api.reuse.software/badge/{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
..    :target: https://api.reuse.software/info/{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}
{%- endif %}
