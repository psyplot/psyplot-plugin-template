.. _installation:

Installation
============

.. warning::

   This page has been automatically generated as has not yet been reviewed by the
   authors of {{ cookiecutter.project_slug }}!

To install the `{{ cookiecutter.project_slug }}` package, we recommend that
you install it from PyPi via::

    pip install {{ cookiecutter.project_slug }}

Or install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git

The latter should however only be done if you want to access the development
versions.

.. _the source code repository on Gitlab: https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.
