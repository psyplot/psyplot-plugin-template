.. _contributing:

Contribution and development hints
==================================

See :ref:`psyplots contribution guidelines <psyplot:contributing>`.
