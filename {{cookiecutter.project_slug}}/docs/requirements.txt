sphinx-design
git+https://codebase.helmholtz.cloud/hcdc/hereon-netcdf/sphinxext.git
git+https://codebase.helmholtz.cloud/psyplot/psyplot.git@develop
{%- if cookiecutter.requires_gui == "yes" %}
git+https://codebase.helmholtz.cloud/psyplot/psyplot-gui.git@develop
{% endif %}
