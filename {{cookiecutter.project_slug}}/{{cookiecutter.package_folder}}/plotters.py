"""plotters module of the {{ cookiecutter.project_slug }} psyplot plugin

This module defines the plotters for the {{ cookiecutter.project_slug }} 
package. It should import all requirements and define the formatoptions and 
plotters that are specified in the 
:mod:`{{ cookiecutter.package_folder }}.plugin` module.
"""

from psyplot.plotter import Formatoption, Plotter


# -----------------------------------------------------------------------------
# ---------------------------- Formatoptions ----------------------------------
# -----------------------------------------------------------------------------


class MyNewFormatoption(Formatoption):

    def update(self, value):
        # hooray
        pass


# -----------------------------------------------------------------------------
# ------------------------------ Plotters -------------------------------------
# -----------------------------------------------------------------------------


class MyPlotter(Plotter):

    _rcparams_string = ['plotter.{{ cookiecutter.package_folder }}.']

    my_fmt = MyNewFormatoption('my_fmt')
